package com.as.training.hw14.pageObject.pages;

import com.as.training.hw14.pageObject.tests.BaseTest;
import com.as.training.hw14.pageObject.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class DraftsPage extends Page {
    private final By drafts = By.xpath ( "//div[@class='TN bzz aHS-bnq']" );
    public final By mail = By.xpath ( String.format ( "//span[@class='bog']//span[contains(text(),'%s')]", BaseTest.SUBJECT ) );
    private final By subject = By.name ( "subjectbox" );
    private final By textBody = By.cssSelector ( "div[class='Am Al editable LW-avf']" );
    private final By addresseeText = By.cssSelector ( "div[class='oL aDm az9']" );
    private final By closeButton = By.className ( "Ha" );


    public DraftsPage openMail() {
        Waiter.waitForElementVisible ( mail );
        driver.findElement ( mail ).click ();
        Waiter.waitForElementClickable ( subject );
        return new DraftsPage ( driver );
    }

    public DraftsPage closeMail() {
        driver.findElement ( closeButton ).click ();
        return new DraftsPage ( driver );
    }

    public DraftsPage sendMail() {
        driver.findElement ( textBody ).click ();
        driver.findElement ( textBody ).sendKeys ( Keys.chord ( Keys.CONTROL, Keys.ENTER ) );
        Waiter.waitForElementClickable ( By.className ( "bAq" ) );
        return new DraftsPage ( driver );
    }

    public String getTextFromAddressee() {
        return driver.findElement ( addresseeText ).getText ();
    }

    public String getTextFromSubject() {
        return driver.findElement ( subject ).getAttribute ( "value" );
    }

    public String getTextFromBody() {
        return driver.findElement ( textBody ).getText ();
    }

    public DraftsPage goToDrafts() {
        Waiter.waitForElementClickable ( drafts );
        driver.findElement ( drafts ).click ();
        return new DraftsPage ( driver );
    }

    public DraftsPage(WebDriver driver) {
        super ( driver );
    }
}
