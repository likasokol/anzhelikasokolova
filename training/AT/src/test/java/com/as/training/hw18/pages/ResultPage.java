package com.as.training.hw18.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

public class ResultPage extends Page {
    public static final By listOfProducts = By.xpath("//a[@class='s-item__link']");

    public ProductPage clickOnFirstProduct() {
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", driver.findElements(listOfProducts).get(0));
        }
        driver.findElements(listOfProducts).get(0).click();
        return new ProductPage();
    }

    public String getTextFromFirstProduct() {
        return driver.findElements(listOfProducts).get(0).getText();
    }
}
