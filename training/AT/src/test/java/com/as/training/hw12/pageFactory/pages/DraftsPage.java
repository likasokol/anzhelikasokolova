package com.as.training.hw12.pageFactory.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;

public class DraftsPage extends Page {
    @FindBy(xpath = "//div[@class='TN bzz aHS-bnq']")
    private WebElement drafts;

    @FindBy(xpath = "//span[@class='bog']//span[contains(text(),'Something important')]")
    private WebElement mail;

    @FindBy(name = "to")
    private WebElement addressee;

    @FindBy(name = "subjectbox")
    private WebElement subject;

    @FindBy(css = "div[class='Am Al editable LW-avf']")
    private WebElement textBody;

    @FindBy(css = "div[class='oL aDm az9']")
    private WebElement addresseeText;

    @FindBy(css = "div[class='T-I J-J5-Ji aoO v7 T-I-atl L3 T-I-Zf-aw2']")
    private WebElement sendButton;

    @FindBy(xpath = "//div[@class='TN bzz aHS-bnu']")
    private WebElement sent;

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public DraftsPage goToDrafts() {
        drafts.click();
        waitForElementVisibility(mail);
        return new DraftsPage(driver);
    }

    public DraftsPage openMail() {
        waitForElementVisibility(mail);
        mail.click();
        return new DraftsPage(driver);
    }

    public String getTextFromSubject() {
        waitForElementVisibility(subject);
        return subject.getAttribute("value");
    }

    public String getTextFromBody() {
        return textBody.getText();
    }

    public String getTextFromAddressee() {
        return addresseeText.getText();
    }

    public String getTime() {
        return driver.findElements(By.xpath("//td[@class='xW xY ']")).get(0).getText();
    }

    public DraftsPage sendMail() {
        textBody.click();
        textBody.sendKeys(Keys.chord(Keys.CONTROL, Keys.ENTER));
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='bAq']")));
        return new DraftsPage(driver);
    }
}
