package com.as.training.hw16;

import com.as.training.hw16.pages.ProductPage;
import org.openqa.selenium.WebElement;

public class Service {
    public ProductPage page;

    public Service() {
        page = new ProductPage();
    }

    public boolean elementIsDisplayed(WebElement element) {
        return element.isDisplayed();
    }
}
