package com.as.training.hw10;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class PositiveOrNegativeNumberTest {
    private long param;
    private Calculator calculator;

    public PositiveOrNegativeNumberTest(long param) {
        this.param = param;
    }

    @Factory
    public static Object[] factoryMethod() {
        return new Object[]{new PositiveOrNegativeNumberTest(-4627727L),
                new PositiveOrNegativeNumberTest(3746737457L),
                new PositiveOrNegativeNumberTest(0L),
                new PositiveOrNegativeNumberTest(3462456453657567L)};
    }

    @BeforeClass
    private void initial() {
        calculator = new Calculator();
    }

    @Test
    private void testIsPositive() {
        boolean result = calculator.isPositive(param);
        Assert.assertEquals(result, param > 0);
    }

    @Test(dependsOnMethods = "testIsPositive")
    private void testIsNegative() {
        boolean result = calculator.isNegative(param);
        Assert.assertEquals(result, param < 0);
    }

    @AfterClass
    private void removeInstance() {
        calculator = null;
    }
}
