package com.as.training.hw12.pageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {
    private final By loginField = By.id("identifierId");
    private final By passwordField = By.name("password");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public IncomingMessagesPage login(String login, String password) {
        driver.findElement(loginField).sendKeys(login);
        driver.findElement(loginField).sendKeys(Keys.ENTER);
        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(passwordField).sendKeys(Keys.ENTER);
        return new IncomingMessagesPage(driver);
    }
}
