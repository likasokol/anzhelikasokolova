package com.as.training.hw14.pageObject.tests;

import com.as.training.hw14.pageObject.businessObjects.Mail;
import com.as.training.hw14.pageObject.businessObjects.User;
import com.as.training.hw14.pageObject.pages.IncomingMessagesPage;
import com.as.training.hw14.pageObject.pages.LoginPage;
import com.as.training.hw14.pageObject.utils.ElementChecker;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

public class BaseTest {
    protected WebDriver driver;
    private static final String TEST_URL = "https://www.gmail.com/";
    private static final String LOGIN = "likasokolova4@gmail.com";
    private static final String PASSWORD = "1234Qwer!";
    private static final String ADDRESSEE = "lika.sokolova23071999@gmail.com";
    public static final String SUBJECT = RandomStringUtils.random ( 16, true, true );
    private static final String BODY = RandomStringUtils.random ( 40, true, true );
    protected Mail mail;

    @BeforeClass
    public void setUp() {
        if (driver == null) {
            WebDriverManager.chromedriver ().setup ();
            driver = new ChromeDriver ();
            driver.manage ().timeouts ().implicitlyWait ( 40, TimeUnit.SECONDS );
            driver.manage ().window ().maximize ();
            driver.get ( TEST_URL );
            User user = new User ( LOGIN, PASSWORD );
            IncomingMessagesPage incomingMessagesPage = new LoginPage ( driver ).login ( user );
            ElementChecker elementChecker = new ElementChecker ( driver );
            Assert.assertTrue ( elementChecker.elementIsPresent ( incomingMessagesPage.createMailButton ) );
            mail = new Mail ( ADDRESSEE, SUBJECT, BODY );
        }
    }

    @AfterClass
    public void reset() {
        driver.findElement ( By.xpath ( "//span[@class='gb_xa gbii']" ) ).click ();
        driver.findElement ( By.id ( "gb_71" ) ).click ();
        driver.quit ();
        driver = null;
    }
}
