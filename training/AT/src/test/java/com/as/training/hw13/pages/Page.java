package com.as.training.hw13.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {
    private final By checkBoxRadio = By.xpath("//a[contains(text(),'Checkboxradio')]");
    private final By dialogLink = By.xpath("//a[contains(text(),'Dialog')]");
    private final By droppable = By.xpath("//a[contains(text(),'Droppable')]");
    private final By selectMenu = By.xpath("//a[contains(text(),'Selectmenu')]");
    private final By tooltip = By.xpath("//a[contains(text(),'Tooltip')]");
    private final By frame = By.className("demo-frame");
    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public CheckBoxRadioPage goToCheckBoxRadio() {
        driver.findElement(checkBoxRadio).click();
        return new CheckBoxRadioPage(driver);
    }

    public DialogPage goToDialog() {
        driver.findElement(dialogLink).click();
        return new DialogPage(driver);
    }

    public DroppablePage goToDroppable() {
        driver.findElement(droppable).click();
        return new DroppablePage(driver);
    }

    public SelectMenuPage goToSelectMenu() {
        driver.findElement(selectMenu).click();
        return new SelectMenuPage(driver);
    }

    public TooltipPage goToTooltip() {
        driver.findElement(tooltip).click();
        return new TooltipPage(driver);
    }

    public void switchToFrame() {
        driver.switchTo().frame(driver.findElement(frame));
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
