package com.as.training.hw15.businessObjects;

public class User {
    private final String name;
    private final String password;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public static User create() {
        return new User("lika.sokolova4@gmail.com", "1234Qwer!");
    }
}
