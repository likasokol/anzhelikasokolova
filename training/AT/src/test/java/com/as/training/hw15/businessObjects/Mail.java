package com.as.training.hw15.businessObjects;

import org.apache.commons.lang3.RandomStringUtils;

public class Mail {
    private String addressee;
    public String subject;
    private String body;

    public Mail(String addressee, String subject, String body) {
        this.addressee = addressee;
        this.subject = subject;
        this.body = body;
    }

    public String getAddressee() {
        return addressee;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public static Mail create() {
        return new Mail("lika.sokolova23071999@gmail.com", RandomStringUtils.random(16, true, true), RandomStringUtils.random(40, true, true));
    }
}
