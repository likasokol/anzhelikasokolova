package com.as.training.hw13.tests;

import com.as.training.hw13.pages.DialogPage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DialogTest extends BaseTest {
    @Test
    public void testDialog() {
        DialogPage dialogPage = new DialogPage(driver).goToDialog().move().close();
        Assert.assertFalse(driver.findElement(dialogPage.closeButton).isDisplayed());
    }
}
