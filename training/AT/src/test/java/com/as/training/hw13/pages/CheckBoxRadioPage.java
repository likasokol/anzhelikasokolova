package com.as.training.hw13.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class CheckBoxRadioPage extends Page {
    public final By radioButton = By.xpath("/html/body/div/fieldset[1]/label[1]");
    public final By checkBox = By.xpath("/html/body/div/fieldset[2]/label[1]");

    public CheckBoxRadioPage(WebDriver driver) {
        super(driver);
    }

    public CheckBoxRadioPage clickOnCheckBoxAndRadioButton() {
        switchToFrame();
        new Actions(driver).click(driver.findElement(radioButton)).build().perform();
        new Actions(driver).click(driver.findElement(checkBox)).build().perform();
        return new CheckBoxRadioPage(driver);
    }
}
