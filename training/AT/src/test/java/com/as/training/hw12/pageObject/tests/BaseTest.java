package com.as.training.hw12.pageObject.tests;

import com.as.training.hw12.pageObject.pages.IncomingMessagesPage;
import com.as.training.hw12.pageObject.pages.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected WebDriver driver;
    private static final String TEST_URL = "https://www.gmail.com/";
    private static final String LOGIN = "likasokolova4@gmail.com";
    private static final String PASSWORD = "1234Qwer!";
    protected static final String ADDRESSEE = "lika.sokolova23071999@gmail.com";
    public static final String SUBJECT = RandomStringUtils.random(16, true, true);
    protected static final String BODY = RandomStringUtils.random(40, true, true);

    @BeforeClass
    public void setUp() {
        if (driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(TEST_URL);
            IncomingMessagesPage incomingMessagesPage = new LoginPage(driver).login(LOGIN, PASSWORD);
            Assert.assertTrue(incomingMessagesPage.elementIsPresent(By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']")));
        }
    }

    @AfterClass
    public void reset() {
        driver.findElement(By.xpath("//span[@class='gb_xa gbii']")).click();
        driver.findElement(By.id("gb_71")).click();
        driver.quit();
        driver = null;
    }
}
