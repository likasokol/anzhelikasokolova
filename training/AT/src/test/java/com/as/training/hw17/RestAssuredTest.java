package com.as.training.hw17;

import com.jayway.restassured.http.ContentType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;

public class RestAssuredTest {
    private static final String URI = "http://jsonplaceholder.typicode.com/albums";
    private String body;

    @BeforeMethod
    private void init() {
        body = "{\"userId\": %s, \"title\": %s}";
    }

    @Test
    public void verifyStatusCode() {
        when().get(URI)
                .then().assertThat().statusCode(200);
    }

    @Test
    public void verifyHeader() {
        when().get(URI)
                .then().assertThat().headers("Content-Type", "application/json; charset=utf-8");
    }

    @Test
    public void verifyBody() {
        when().get(URI)
                .then().assertThat().body("size()", is(100));
    }

    @Test
    public void verifyAlbumById() {
        when().get(URI + "/4")
                .then().assertThat().body("$", hasKey("userId"), "$", hasKey("id"), "$", hasKey("title"));
    }

    @Test
    public void verifyAddNewAlbum() {
        int id = given().body(String.format(body, "1", "test album"))
                .when().post(URI)
                .then().assertThat().statusCode(201).contentType(ContentType.JSON).extract().path("id");
        System.out.println(id);
    }

    @Test
    public void verifyUpdateAlbum() {
        given().body(String.format(body, "1", "test album update"))
                .when().put(URI + "/100")
                .then().assertThat().statusCode(200).body("id", is(100));
    }

    @Test
    public void verifyDeleteAlbum() {
        when().delete(URI + "/100")
                .then().assertThat().statusCode(200);
    }
}
