package com.as.training.hw15.pages;

import com.as.training.hw15.businessObjects.Mail;
import com.as.training.hw15.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InboxPage extends Page {
    public final By createMailButton = By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']");
    private final By addressee = By.name("to");
    private final By subject = By.name("subjectbox");
    private final By textBody = By.cssSelector("div[class='Am Al editable LW-avf']");
    private final By saveAsDraftButton = By.className("Ha");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public InboxPage createMail(Mail mail) {
        driver.findElement(createMailButton).click();
        return setInfo(mail);
    }

    public InboxPage saveAsDraft(Mail mail) {
        Waiter.waitForElementVisible(By.xpath("//div[contains(string(), 'Черновик сохранен')]"));
        Waiter.waitForElementVisible(By.xpath(String.format("//div[contains(string(), '%s')]", mail.subject)));
        driver.findElement(saveAsDraftButton).click();
        return new InboxPage(driver);
    }

    public InboxPage setInfo(Mail mail) {
        Waiter.waitForElementClickable(addressee);
        driver.findElement(addressee).sendKeys(mail.getAddressee());
        driver.findElement(subject).sendKeys(mail.getSubject());
        driver.findElement(textBody).sendKeys(mail.getBody());
        return new InboxPage(driver);
    }
}