package com.as.training.hw18.pages;

import com.as.training.hw18.browser.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {
    protected WebDriver driver = Browser.getWebDriverInstance ();

    public void waitForElementVisible(By locator) {
        new WebDriverWait ( driver, 30 ).until ( ExpectedConditions.visibilityOfElementLocated ( locator ) );
    }
}
