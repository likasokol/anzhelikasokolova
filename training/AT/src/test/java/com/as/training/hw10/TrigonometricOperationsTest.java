package com.as.training.hw10;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Random;

public class TrigonometricOperationsTest {
    private Calculator calculator;
    private Random random = new Random();

    @BeforeClass
    private void initial() {
        calculator = new Calculator();
    }

    @DataProvider
    private Object[][] createData() {
        return new Object[][]{{0}, {30}, {45}, {60}, {90}, {180}, {270}, {360}, {random.nextInt(360)}};
    }


    @Test(dataProvider = "createData")
    private void testSin(double number) {
        double result = calculator.sin(number);
        Assert.assertEquals(result, Math.sin(number), 0.0009);
    }

    @Test(dataProvider = "createData")
    private void testCos(double number) {
        double result = calculator.cos(number);
        Assert.assertEquals(result, Math.cos(number), 0.0009);
    }

    @Test(dataProvider = "createData")
    private void testTg(double number) {
        double result = calculator.tg(number);
        Assert.assertEquals(result, Math.tan(number), 0.0009);
    }

    @Test(dataProvider = "createData")
    private void testCtg(double number) {
        double result = calculator.ctg(number);
        Assert.assertEquals(result, 1.0 / Math.tan(number), 0.0009);
    }

    @AfterClass
    private void removeInstance() {
        calculator = null;
    }
}
