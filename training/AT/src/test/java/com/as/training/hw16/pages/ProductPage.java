package com.as.training.hw16.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ProductPage extends Page {
    private final By addButton = By.cssSelector("#isCartBtn_btn");
    public WebElement addButtonElement = driver.findElement(addButton);

    public CartPage addToCart() {
        driver.findElement(addButton).click();
        return new CartPage();
    }
}
