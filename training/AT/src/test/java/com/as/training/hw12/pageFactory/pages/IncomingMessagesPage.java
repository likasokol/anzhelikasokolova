package com.as.training.hw12.pageFactory.pages;

import com.as.training.hw12.pageFactory.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IncomingMessagesPage extends Page {
    public IncomingMessagesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji T-I-KE L3']")
    private WebElement createMailButton;

    @FindBy(name = "to")
    private WebElement addressee;

    @FindBy(name = "subjectbox")
    private WebElement subject;

    @FindBy(css = "div[class='Am Al editable LW-avf']")
    private WebElement textBody;

    @FindBy(className = "Ha")
    private WebElement saveAsDraftButton;

    @FindBy(xpath = "//div[@class='TN bzz aHS-bnq']")
    private WebElement drafts;

    @FindBy(className = "bog")
    private WebElement mail;

    public IncomingMessagesPage setInfo(String addresseeText, String subjectText, String bodyText) {
        addressee.sendKeys(addresseeText);
        subject.sendKeys(subjectText);
        textBody.sendKeys(bodyText);
        return new IncomingMessagesPage(driver);
    }

    public IncomingMessagesPage createMail(String addresseeText, String subjectText, String bodyText) {
        createMailButton.click();
        IncomingMessagesPage incomingMessagesPage = setInfo(addresseeText, subjectText, bodyText);

        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(string(), 'Черновик сохранен')]")));
        BaseTest baseTest = new BaseTest();
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(String.format("//div[contains(string(), '%s')]", baseTest.SUBJECT))));
        saveAsDraftButton.click();
        return incomingMessagesPage;
    }
}
