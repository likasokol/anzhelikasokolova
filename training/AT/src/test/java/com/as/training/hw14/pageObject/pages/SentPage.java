package com.as.training.hw14.pageObject.pages;

import com.as.training.hw14.pageObject.tests.BaseTest;
import com.as.training.hw14.pageObject.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentPage extends Page {
    private final By sent = By.xpath ( "//div[@class='TN bzz aHS-bnu']" );
    public final By mail = By.xpath ( String.format ( "//span[@class='bog']//span[contains(text(),'%s')]", BaseTest.SUBJECT ) );
    public final By addresseeText = By.xpath ( "//span[@class='g2']" );
    public final By subject = By.xpath ( "//h2[@class='hP']" );
    public final By textBody = By.xpath ( "//div[@class='a3s aXjCH ']" );

    public SentPage goToSent() {
        driver.findElement ( sent ).click ();
        return new SentPage ( driver );
    }

    public SentPage(WebDriver driver) {
        super ( driver );
    }

    public SentPage openMail() {
        driver.findElement ( mail ).click ();
        return new SentPage ( driver );
    }

    public String getTextFromAddressee() {
        Waiter.waitForElementClickable ( addresseeText );
        return driver.findElement ( addresseeText ).getText ();
    }

    public String getTextFromSubject() {
        return driver.findElement ( subject ).getText ();
    }

    public String getTextFromBody() {
        return driver.findElement ( textBody ).getText ();
    }
}
