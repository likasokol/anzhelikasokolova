package com.as.training.hw13.tests;

import com.as.training.hw13.pages.DroppablePage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DroppableTest extends BaseTest {
    @Test
    public void testDragAndDrop() {
        DroppablePage droppablePage = new DroppablePage(driver).goToDroppable().dragAndDropSquare();
        WebElement square = driver.findElement(droppablePage.bigSquare);
        String squareClassName = square.getAttribute("class");
        Assert.assertEquals(squareClassName, "ui-widget-header ui-droppable ui-state-highlight");
    }
}
