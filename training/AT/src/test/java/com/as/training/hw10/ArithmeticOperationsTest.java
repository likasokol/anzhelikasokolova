package com.as.training.hw10;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ArithmeticOperationsTest {
    private Calculator calculator;

    @BeforeClass
    private void initial() {
        calculator = new Calculator();
    }

    @Test
    private void testLongSum() {
        long result = calculator.sum(2147483648L, 2147483648L);
        Assert.assertEquals(result, 4294967296L);
    }

    @Test(priority = 1)
    private void testDoubleSum() {
        double result = calculator.sum(4.4, 0.001);
        Assert.assertEquals(result, 4.401);
    }

    @Test(priority = 3)
    private void testLongSub() {
        long result = calculator.sub(2147483649L, 2147483648L);
        Assert.assertEquals(result, 1L);
    }

    @Test(priority = 2)
    private void testDoubleSub() {
        double result = calculator.sub(400, 399.01);
        Assert.assertEquals(result, 0.99);
    }

    @Test(priority = 10)
    private void testLongMult() {
        long result = calculator.mult(2147483648L, 2147483648L);
        Assert.assertEquals(result, 4611686018427387904L);
    }

    @Test(priority = 9)
    private void testDoubleMult() {
        double result = calculator.mult(0.1, 9);
        Assert.assertEquals(result, 0.9);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    private void testLongDiv() {
        calculator.div(2147483648L, 0L);
    }

    @Test(expectedExceptions = NumberFormatException.class)
    private void testDoubleDiv() {
        calculator.div(4, 0.0);
    }

    @Test(priority = 5)
    private void testPow() {
        double result = calculator.pow(2, 0.5);
        Assert.assertEquals(result, 1.0);
    }

    @Test(priority = 4)
    private void testSqrt() {
        double result = calculator.sqrt(-4);
        Assert.assertEquals(result, 2.0);
    }

    @AfterClass
    private void removeInstance() {
        calculator = null;
    }
}
