package com.as.training.hw15.pages;

import com.as.training.hw15.tests.BaseTest;
import com.as.training.hw15.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class DraftsPage extends Page {
    private final By drafts = By.xpath("//div[@class='TN bzz aHS-bnq']");
    public final By mail = By.xpath(String.format("//span[@class='bog']//span[contains(text(),'%s')]", BaseTest.mail.getSubject()));
    public final By firstMail = By.xpath("//span[@class='bog']");
    private final By subject = By.name("subjectbox");
    private final By textBody = By.cssSelector("div[class='Am Al editable LW-avf']");
    private final By addresseeText = By.cssSelector("div[class='oL aDm az9']");
    private final By saveAsDraftButton = By.className("Ha");


    public DraftsPage openMail() {
        Waiter.waitForElementVisible(mail);
        driver.findElement(mail).click();
        return new DraftsPage(driver);
    }

    public DraftsPage openFirstMail() {
        Waiter.waitForElementVisible(firstMail);
        driver.findElement(firstMail).click();
        return new DraftsPage(driver);
    }

    public DraftsPage closeMail() {
        driver.findElement(saveAsDraftButton).click();
        return new DraftsPage(driver);
    }

    public DraftsPage sendMail() {
        driver.findElement(textBody).click();
        driver.findElement(textBody).sendKeys(Keys.chord(Keys.CONTROL, Keys.ENTER));
        return new DraftsPage(driver);
    }

    public String getTextFromAddressee() {
        Waiter.waitForElementClickable(addresseeText);
        return driver.findElement(addresseeText).getText();
    }

    public String getTextFromSubject() {
        return driver.findElement(subject).getAttribute("value");
    }

    public String getTextFromBody() {
        return driver.findElement(textBody).getText();
    }

    public DraftsPage goToDrafts() {
        Waiter.waitForElementClickable(drafts);
        driver.findElement(drafts).click();
        return new DraftsPage(driver);
    }

    public DraftsPage(WebDriver driver) {
        super(driver);
    }
}
