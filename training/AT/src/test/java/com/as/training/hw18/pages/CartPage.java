package com.as.training.hw18.pages;

import org.openqa.selenium.By;

public class CartPage extends Page{
    private By product = By.xpath("//span[@class='BOLD']");

    public String getTextFromProduct() {
        return driver.findElement(product).getText();
    }
}
