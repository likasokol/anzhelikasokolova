package com.as.training.hw14.pageObject.tests;

import com.as.training.hw14.pageObject.pages.DraftsPage;
import com.as.training.hw14.pageObject.pages.IncomingMessagesPage;
import com.as.training.hw14.pageObject.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DraftsTest extends BaseTest {

    @Test
    public void testMailIsPresent() {
        new IncomingMessagesPage ( driver ).createMail ( mail ).saveAsDraft ();
        DraftsPage draftsPage = new DraftsPage ( driver ).goToDrafts ().openMail ();
        Assert.assertEquals ( draftsPage.getTextFromAddressee (), mail.getAddressee () );
        Assert.assertEquals ( draftsPage.getTextFromSubject (), mail.getSubject () );
        Assert.assertEquals ( draftsPage.getTextFromBody (), mail.getBody () );
        draftsPage.sendMail ();
    }
}
