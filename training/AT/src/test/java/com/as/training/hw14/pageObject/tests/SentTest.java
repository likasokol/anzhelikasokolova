package com.as.training.hw14.pageObject.tests;

import com.as.training.hw14.pageObject.pages.DraftsPage;
import com.as.training.hw14.pageObject.pages.IncomingMessagesPage;
import com.as.training.hw14.pageObject.pages.SentPage;
import com.as.training.hw14.pageObject.utils.ElementChecker;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SentTest extends BaseTest {

    @Test
    public void testMailContent() {
        new IncomingMessagesPage ( driver ).createMail ( mail ).saveAsDraft ();
        DraftsPage draftsPage = new DraftsPage ( driver ).goToDrafts ().openMail ();
        draftsPage.sendMail ();
        SentPage sentPage = new SentPage ( driver ).goToSent ().openMail ();
        Assert.assertTrue ( mail.getAddressee ().contains ( sentPage.getTextFromAddressee () ));
        Assert.assertEquals ( sentPage.getTextFromSubject (), mail.getSubject () );
        Assert.assertEquals ( sentPage.getTextFromBody (), mail.getBody () );
    }
}
