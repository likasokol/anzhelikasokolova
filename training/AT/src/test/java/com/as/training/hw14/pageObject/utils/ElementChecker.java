package com.as.training.hw14.pageObject.utils;

import com.as.training.hw14.pageObject.pages.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ElementChecker extends Page {
    public ElementChecker(WebDriver driver) {
        super(driver);
    }

    public static boolean elementIsPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }
}
