package com.as.training.hw13.tests;

import com.as.training.hw13.pages.SelectMenuPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SelectMenuTest extends BaseTest {
    private static final String SPEED = "Fast";
    private static final String NUMBER = "3";

    @Test
    public void testSelect() {
        SelectMenuPage selectMenuPage = new SelectMenuPage(driver).goToSelectMenu().select();
        Assert.assertEquals(selectMenuPage.getTextFromSpeed(), SPEED);
        Assert.assertEquals(selectMenuPage.getTextFromNumber(), NUMBER);
    }
}
