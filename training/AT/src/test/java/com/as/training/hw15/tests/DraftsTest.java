package com.as.training.hw15.tests;

import com.as.training.hw15.businessObjects.*;
import com.as.training.hw15.pages.DraftsPage;
import com.as.training.hw15.pages.InboxPage;
import com.as.training.hw15.utils.Waiter;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DraftsTest extends BaseTest {
    @Test
    public void testMailIsPresent() {
        new InboxPage(Browser.getInstance()).createMail(mail).saveAsDraft(mail);
        DraftsPage draftsPage = new DraftsPage(Browser.getInstance()).goToDrafts();
        Assert.assertTrue(Browser.getInstance().findElement(draftsPage.mail).isDisplayed());
    }

    @Test
    public void testMailContent() {
        new InboxPage(Browser.getInstance()).createMail(mail).saveAsDraft(mail);
        DraftsPage draftsPage = new DraftsPage(Browser.getInstance()).goToDrafts().openMail();
        Assert.assertEquals(draftsPage.getTextFromAddressee(), mail.getAddressee());
        Assert.assertEquals(draftsPage.getTextFromSubject(), mail.getSubject());
        Assert.assertEquals(draftsPage.getTextFromBody(), mail.getBody());
        new DraftsPage(Browser.getInstance()).goToDrafts().closeMail();
    }

    @Test
    public void testSendMail() {
        new InboxPage(Browser.getInstance()).createMail(mail).saveAsDraft(mail);
        DraftsPage draftsPage = new DraftsPage(Browser.getInstance()).goToDrafts().openMail().sendMail();
        Waiter.waitForElementVisible(By.xpath("//span[@class='bAq']"));
        draftsPage.openFirstMail();
        Assert.assertNotEquals(draftsPage.getTextFromSubject(), BaseTest.mail.subject);
    }
}