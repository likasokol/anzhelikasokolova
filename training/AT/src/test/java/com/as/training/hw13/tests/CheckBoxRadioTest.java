package com.as.training.hw13.tests;

import com.as.training.hw13.pages.CheckBoxRadioPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckBoxRadioTest extends BaseTest {
    @Test
    public void testCheckBoxRadio() {
        CheckBoxRadioPage checkBoxRadioPage = new CheckBoxRadioPage(driver).goToCheckBoxRadio().clickOnCheckBoxAndRadioButton();
        WebElement radioButton = driver.findElement(checkBoxRadioPage.radioButton);
        String radioClassName = radioButton.getAttribute("class");
        WebElement checkBox = driver.findElement(checkBoxRadioPage.checkBox);
        String checkClassName = checkBox.getAttribute("class");
        Assert.assertEquals(radioClassName, "ui-checkboxradio-label ui-corner-all ui-button ui-widget ui-checkboxradio-radio-label ui-checkboxradio-checked ui-state-active");
        Assert.assertEquals(checkClassName, "ui-checkboxradio-label ui-corner-all ui-button ui-widget ui-state-focus ui-visual-focus ui-checkboxradio-checked ui-state-active");
    }
}
