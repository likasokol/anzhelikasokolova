package com.as.training.hw13.tests;

import com.as.training.hw13.pages.TooltipPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TooltipTest extends BaseTest {
    private static final String TITLE = "That's what this widget is";

    @Test
    public void testTooltip() {
        TooltipPage tooltipPage = new TooltipPage(driver).goToTooltip();
        Assert.assertEquals(tooltipPage.getTextFromTitle(), TITLE);
    }
}
