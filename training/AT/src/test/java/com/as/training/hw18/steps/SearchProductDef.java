package com.as.training.hw18.steps;

import com.as.training.hw18.TestListener;
import com.as.training.hw18.browser.Browser;
import com.as.training.hw18.pages.CartPage;
import com.as.training.hw18.pages.HomePage;
import com.as.training.hw18.pages.ProductPage;
import com.as.training.hw18.pages.ResultPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;

@Listeners(TestListener.class)
public class SearchProductDef {
    private Logger logger = LogManager.getLogger(this.getClass());

    @Given("^I opened Ebay Page$")
    public void iOpenedEbayPage() {
        new HomePage().open();
        logger.info("Go to the home page ");
        takeScreenshot("screenshot1");
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new HomePage().enterQuery(query).clickSubmit();
        logger.info("search product ");
        takeScreenshot("screenshot2");
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectedString) {

        Assert.assertTrue(new ResultPage().getTextFromFirstProduct().contains(expectedString));
        takeScreenshot("screenshot3");
    }

    @And("^I navigate to product page$")
    public void iNavigateToProductPage() {
        new ResultPage().clickOnFirstProduct();
        logger.info("go to the product page ");
        takeScreenshot("screenshot3");
    }

    @Then("^the product page is opened$")
    public void theProductPageIsOpened() {
        Assert.assertTrue(new ProductPage().addButtonElement.isDisplayed());
        takeScreenshot("screenshot4");
    }

    @And("^I add product to cart$")
    public void iAddProductToCart() {
        new ProductPage().addToCart();
        logger.info("add product to cart ");
        takeScreenshot("screenshot5");
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Cart grid$")
    public void theTermQueryShouldBeTheFirstInTheCartGrid(String expectedString) {
        Assert.assertTrue(new CartPage().getTextFromProduct().contains(expectedString));
        takeScreenshot("screenshot6");
    }

    private static void takeScreenshot(String name) {
        File scrFile = ((TakesScreenshot) Browser.driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("./screenshots/" + name + ".png/"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
