package com.as.training.hw15.tests;

import com.as.training.hw15.businessObjects.Browser;
import com.as.training.hw15.businessObjects.Mail;
import com.as.training.hw15.businessObjects.User;
import com.as.training.hw15.pages.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    private static final String TEST_URL = "https://www.gmail.com/";
    private static final String LOGIN = "likasokolova4@gmail.com";
    private static final String PASSWORD = "1234Qwer!";
    public static Mail mail;

    @BeforeClass
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        Browser.getInstance().manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        Browser.getInstance().manage().window().maximize();
        Browser.getInstance().get(TEST_URL);
        User user = User.create();
        mail = Mail.create();
        new LoginPage(Browser.getInstance()).login(user);
    }

    @AfterClass
    public void reset() {
        Browser.getInstance().findElement(By.xpath("//span[@class='gb_ya gbii']")).click();
        Browser.getInstance().findElement(By.id("gb_71")).click();
        Browser.close();
    }
}
