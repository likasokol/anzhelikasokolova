﻿package com.as.training.hw14.selenide;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginTest {
    private static String gmailUsername = System.getProperty("gmail.username", "likasokolova4@gmail.com");
    private static String gmailPassword = System.getProperty("gmail.password", "1234Qwer!");

    @Test
    private static void login() {
        WebDriverManager.chromedriver().setup();
        System.setProperty("selenide.browser", "Chrome");
        open("http://gmail.com");

        $("#identifierId").val(gmailUsername).pressEnter();
        $("#password input").val(gmailPassword).pressEnter();
        $(By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']")).shouldHave(text("Написать"));
    }
}
