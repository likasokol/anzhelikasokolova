package com.as.training.hw13.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class SelectMenuPage extends Page {
    private final By speed = By.xpath("//span[@id='speed-button']");
    private final By fast = By.xpath("//div[@id='ui-id-4']");
    private final By number = By.xpath("//span[@id='number-button']");
    private final By number_3 = By.xpath("//div[@id='ui-id-8']");

    public SelectMenuPage(WebDriver driver) {
        super(driver);
    }

    public SelectMenuPage select() {
        switchToFrame();
        new Actions(driver)
                .click(driver.findElement(speed))
                .click(driver.findElement(fast))
                .click(driver.findElement(number))
                .click(driver.findElement(number_3)).build().perform();
        return new SelectMenuPage(driver);
    }

    public String getTextFromSpeed() {
        return driver.findElement(speed).getText();
    }

    public String getTextFromNumber() {
        return driver.findElement(number).getText();
    }
}
