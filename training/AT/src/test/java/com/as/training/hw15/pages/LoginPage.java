package com.as.training.hw15.pages;

import com.as.training.hw15.businessObjects.User;
import com.as.training.hw15.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {
    private final By loginField = By.id("identifierId");
    private final By passwordField = By.name("password");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public InboxPage login(User user) {
        setLogin(user);
        driver.findElement(loginField).sendKeys(Keys.ENTER);
        setPassword(user);
        driver.findElement(passwordField).sendKeys(Keys.ENTER);
        return new InboxPage(driver);
    }

    private InboxPage setLogin(User user) {
        Waiter.waitForElementClickable(loginField);
        driver.findElement(loginField).sendKeys(user.getName());
        return new InboxPage(driver);
    }

    private InboxPage setPassword(User user) {
        Waiter.waitForElementClickable(passwordField);
        driver.findElement(passwordField).sendKeys(user.getPassword());
        return new InboxPage(driver);
    }
}

