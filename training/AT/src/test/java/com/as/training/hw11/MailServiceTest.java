package com.as.training.hw11;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class MailServiceTest {
    private static final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    private static WebDriver driver;
    private final By loginField = By.id("identifierId");
    private final By passwordField = By.name("password");
    private final By drafts = By.xpath("//a[contains(text(),'Черновики')]");
    private final By addressee = By.name("to");
    private final By subject = By.name("subjectbox");
    private final By textBody = By.cssSelector("div[class='Am Al editable LW-avf']");
    private final By sent = By.xpath("//div[@class='TN bzz aHS-bnu']");
    private final By mail = By.xpath(String.format("//span[@class='bog']//span[contains(text(),'%s')]", SUBJECT_TEXT));
    private final By identifierNext = By.id("identifierNext");
    private final By passwordNext = By.id("passwordNext");
    private static final String SUBJECT_TEXT = "Something important";
    private static final String BODY_TEXT = "Hello";
    private static final String ADDRESSEE = "lika.sokolova23071999@gmail.com";

    @BeforeClass
    private static void setUp() {
        System.setProperty("webdriver.chrome.driver", CHROME_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://gmail.com");
    }

    @Test
    private void login() {
        driver.findElement(loginField).sendKeys("likasokolova4@gmail.com");
        waitForElementClickable(identifierNext);
        driver.findElement(identifierNext).click();
        driver.findElement(passwordField).sendKeys("1234Qwer!");
        waitForElementClickable(passwordNext);
        driver.findElement(passwordNext).click();
        Assert.assertTrue(driver.findElement((By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']"))).isDisplayed());
    }

    @Test(priority = 1)
    private void writeMail() {
        driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']")).click();
        driver.findElement(addressee).sendKeys(ADDRESSEE);
        driver.findElement(subject).sendKeys(SUBJECT_TEXT);
        driver.findElement(textBody).sendKeys(BODY_TEXT);
    }

    @Test(priority = 2)
    private void saveAsDraft() {
        driver.findElement(drafts).click();
        driver.findElement(By.className("Ha")).click();
        waitForElementClickable(mail);
        Assert.assertTrue(driver.findElement(mail).isDisplayed());
    }

    @Test(priority = 3)
    private void sendMail() {
        driver.findElement(mail).click();
        String send = Keys.chord(Keys.CONTROL, Keys.ENTER);
        driver.findElement(textBody).sendKeys(send);
        waitForElementClickable(sent);
        driver.findElement(sent).click();
        Assert.assertTrue(driver.findElement(mail).isDisplayed());
    }

    @AfterClass
    private static void tearDown() {
        driver.findElement(By.xpath("//span[@class='gb_xa gbii']")).click();
        driver.findElement(By.xpath("//a[@id='gb_71']")).click();
        driver.quit();
    }

    private static void waitForElementClickable(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(locator));
    }
}
