package com.as.training.hw15.tests;

import com.as.training.hw15.businessObjects.Browser;
import com.as.training.hw15.pages.InboxPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {
    @Test
    public void testMailIsPresent() {
        InboxPage inboxPage = new InboxPage(Browser.getInstance());
        Assert.assertTrue(Browser.getInstance().findElement(inboxPage.createMailButton).isDisplayed());
    }
}
