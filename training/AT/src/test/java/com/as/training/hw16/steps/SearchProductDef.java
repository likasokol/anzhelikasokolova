package com.as.training.hw16.steps;

import com.as.training.hw16.Service;
import com.as.training.hw16.pages.CartPage;
import com.as.training.hw16.pages.HomePage;
import com.as.training.hw16.pages.ResultPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class SearchProductDef {

    @Given("^I opened Ebay Page$")
    public void iOpenedEbayPage() {
        new HomePage().open();
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new HomePage().enterQuery(query).clickSubmit();
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectedString) {
        Assert.assertTrue(new ResultPage().getTextFromFirstProduct().toLowerCase().contains(expectedString.toLowerCase()));
    }

    @And("^I navigate to product page$")
    public void iNavigateToProductPage() {
        new ResultPage().clickOnFirstProduct();
    }

    @Then("^the product page is opened$")
    public void theProductPageIsOpened() {
        Service service = new Service();
        Assert.assertTrue(service.elementIsDisplayed(service.page.addButtonElement));
    }

    @And("^I add product to cart$")
    public void iAddProductToCart() {
        Service service = new Service();
        service.page.addToCart();
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Cart grid$")
    public void theTermQueryShouldBeTheFirstInTheCartGrid(String expectedString) {
        Assert.assertTrue(new CartPage().getTextFromProduct().toLowerCase().contains(expectedString.toLowerCase()));
    }
}
