package com.as.training.hw12.pageFactory.tests;

import com.as.training.hw12.pageFactory.pages.DraftsPage;
import com.as.training.hw12.pageObject.pages.IncomingMessagesPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DraftsTest extends BaseTest {

    @Test
    public void testMailIsPresent() {
        new IncomingMessagesPage(driver).createMail(ADDRESSEE, SUBJECT, BODY);
        DraftsPage draftsPage = new DraftsPage(driver).goToDrafts();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime time = LocalTime.now();
        String f = formatter.format(time);
        Assert.assertEquals(draftsPage.getTime(), f);
    }

    @Test
    public void testMailContent() {
        new IncomingMessagesPage(driver).createMail(ADDRESSEE, SUBJECT, BODY);
        DraftsPage draftsPage = new DraftsPage(driver).goToDrafts().openMail();
        Assert.assertEquals(draftsPage.getTextFromSubject(), SUBJECT);
        Assert.assertEquals(draftsPage.getTextFromBody(), BODY);
        Assert.assertEquals(draftsPage.getTextFromAddressee(), ADDRESSEE);
    }
}
