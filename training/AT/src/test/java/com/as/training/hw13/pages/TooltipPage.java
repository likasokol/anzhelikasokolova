package com.as.training.hw13.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class TooltipPage extends Page {
    private final By link = By.xpath("//a[contains(text(),'Tooltips')]");

    public TooltipPage(WebDriver driver) {
        super(driver);
    }

    public String getTextFromTitle() {
        switchToFrame();
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(link)).build().perform();
        WebElement toolTipElement = driver.findElement(By.cssSelector(".ui-tooltip"));
        return toolTipElement.getText();
    }
}
