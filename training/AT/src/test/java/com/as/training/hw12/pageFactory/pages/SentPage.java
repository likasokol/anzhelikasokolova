package com.as.training.hw12.pageFactory.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SentPage extends Page {
    @FindBy(xpath = "//div[@class='TN bzz aHS-bnu']")
    private WebElement sent;

    @FindBy(className = "bog")
    private WebElement mail;

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public String getTime() {
        return driver.findElements(By.xpath("//td[@class='xW xY ']")).get(0).getText();
    }

    public SentPage goToSent() {
        waitForElementVisibility(sent);
        sent.click();
        waitForElementVisibility(mail);
        return new SentPage(driver);
    }
}
