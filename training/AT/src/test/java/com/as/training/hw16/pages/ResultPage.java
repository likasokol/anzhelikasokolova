package com.as.training.hw16.pages;

import org.openqa.selenium.By;

public class ResultPage extends Page {
    private final By listOfProducts = By.xpath("//a[@class='s-item__link']");

    public ProductPage clickOnFirstProduct() {
        driver.findElements(listOfProducts).get(0).click();
        return new ProductPage();
    }

    public String getTextFromFirstProduct() {
        return driver.findElements(listOfProducts).get(0).getText();
    }
}
