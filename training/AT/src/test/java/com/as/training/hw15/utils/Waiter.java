package com.as.training.hw15.utils;

import com.as.training.hw15.pages.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter extends Page {
    public Waiter(WebDriver driver) {
        super(driver);
    }

    public static void waitForElementClickable(By locator) {
        new WebDriverWait(driver, 40).until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static void waitForElementVisible(By locator) {
        new WebDriverWait(driver, 40).until(ExpectedConditions.presenceOfElementLocated(locator));
    }
}