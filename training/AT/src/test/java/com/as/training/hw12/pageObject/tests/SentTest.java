package com.as.training.hw12.pageObject.tests;

import com.as.training.hw12.pageObject.pages.DraftsPage;
import com.as.training.hw12.pageObject.pages.IncomingMessagesPage;
import com.as.training.hw12.pageObject.pages.SentPage;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SentTest extends BaseTest {
    @Test
    public void testMailIsPresent() {
        new IncomingMessagesPage(driver).createMail(ADDRESSEE, SUBJECT, BODY);
        new DraftsPage(driver).goToDrafts().openMail();
        new DraftsPage(driver).sendMail();
        SentPage sentPage = new SentPage(driver).goToSent();
        Assert.assertTrue(sentPage.elementIsPresent(By.className("bog")));
    }
}
