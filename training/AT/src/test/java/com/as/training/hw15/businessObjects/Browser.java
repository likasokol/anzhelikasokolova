package com.as.training.hw15.businessObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browser {
    private static WebDriver driver;

    private Browser() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            driver = new ChromeDriver();
            driver = new DriverDecorator(driver);
        }
        return driver;
    }

    public static void close() {
        driver.quit();
        driver = null;
    }
}
