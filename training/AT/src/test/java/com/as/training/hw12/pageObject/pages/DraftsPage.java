package com.as.training.hw12.pageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class DraftsPage extends Page {
    private final By drafts = By.xpath("//div[@class='TN bzz aHS-bnq']");
    private final By mail = By.className("bog");
    private final By subject = By.name("subjectbox");
    private final By textBody = By.cssSelector("div[class='Am Al editable LW-avf']");
    private final By addresseeText = By.cssSelector("div[class='oL aDm az9']");

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public DraftsPage goToDrafts() {
        driver.findElement(drafts).click();
        waitForElementVisibility(driver.findElement(mail));
        return new DraftsPage(driver);
    }

    public String getTime() {
        return driver.findElements(By.xpath("//td[@class='xW xY ']")).get(0).getText();
    }

    public DraftsPage openMail() {
        goToDrafts();
        waitForElementVisibility(driver.findElement(mail));
        driver.findElement(mail).click();
        return new DraftsPage(driver);
    }

    public String getTextFromSubject() {
        waitForElementVisibility(driver.findElement(subject));
        return driver.findElement(subject).getAttribute("value");
    }

    public String getTextFromBody() {

        return driver.findElement(textBody).getText();
    }

    public String getTextFromAddressee() {

        return driver.findElement(addresseeText).getText();
    }

    public DraftsPage sendMail() {
        driver.findElement(textBody).click();
        driver.findElement(textBody).sendKeys(Keys.chord(Keys.CONTROL, Keys.ENTER));
        return new DraftsPage(driver);
    }
}
