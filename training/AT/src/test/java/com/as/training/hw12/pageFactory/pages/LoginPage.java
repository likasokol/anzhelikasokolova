package com.as.training.hw12.pageFactory.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "identifierId")
    private WebElement loginField;

    @FindBy(name = "password")
    private WebElement passwordField;

    public IncomingMessagesPage login(String login, String password) {
        return setInfo(login, password);
    }

    public IncomingMessagesPage setInfo(String login, String password) {
        loginField.sendKeys(login);
        loginField.sendKeys(Keys.ENTER);
        passwordField.sendKeys(password);
        passwordField.sendKeys(Keys.ENTER);
        return new IncomingMessagesPage(driver);
    }
}
