package com.as.training.hw16.pages;

import org.openqa.selenium.By;

public class HomePage extends Page {
    private static final String BASE_URL = "https://www.ebay.com/";
    private final By searchInput = By.cssSelector("#gh-ac");
    private final By submit = By.cssSelector("#gh-btn");

    public HomePage open() {
        driver.get(BASE_URL);
        return this;
    }

    public HomePage enterQuery(String query) {
        waitForElementVisible(searchInput);
        driver.findElement(searchInput).sendKeys(query);
        return new HomePage();
    }

    public ResultPage clickSubmit() {
        waitForElementVisible(submit);
        driver.findElement(submit).click();
        return new ResultPage();
    }
}
