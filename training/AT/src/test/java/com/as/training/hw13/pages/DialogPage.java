package com.as.training.hw13.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class DialogPage extends Page {
    public final By closeButton = By.xpath("/html/body/div/div[1]/button");
    private final By titleBar = By.xpath("/html/body/div/div[1]");

    public DialogPage(WebDriver driver) {
        super(driver);
    }

    public DialogPage move() {
        switchToFrame();
        waitForElementVisibility(titleBar);
        new Actions(driver).dragAndDropBy(driver.findElement(titleBar), 120, 120).build().perform();
        return new DialogPage(driver);
    }

    public DialogPage close() {
        driver.findElement(closeButton).click();
        return new DialogPage(driver);
    }
}
