package com.as.training.hw13.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class DroppablePage extends Page {
    private final By smallSquare = By.id("draggable");
    public final By bigSquare = By.cssSelector("#droppable");

    public DroppablePage(WebDriver driver) {
        super(driver);
    }

    public DroppablePage dragAndDropSquare() {
        switchToFrame();
        new Actions(driver).dragAndDrop(driver.findElement(smallSquare), driver.findElement(bigSquare)).build().perform();
        return new DroppablePage(driver);
    }
}
