package com.as.training.hw12.pageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentPage extends Page {
    private final By sent = By.xpath("//div[@class='TN bzz aHS-bnu']");
    private final By mail = By.className("bog");

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public SentPage goToSent() {
        driver.findElement(sent).click();
        waitForElementVisibility(driver.findElement(mail));
        return new SentPage(driver);
    }
}
