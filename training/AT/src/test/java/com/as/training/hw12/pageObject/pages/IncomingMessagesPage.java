package com.as.training.hw12.pageObject.pages;

import com.as.training.hw12.pageFactory.tests.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IncomingMessagesPage extends Page {
    private final By createMailButton = By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']");
    private final By addressee = By.name("to");
    private final By subject = By.name("subjectbox");
    private final By textBody = By.cssSelector("div[class='Am Al editable LW-avf']");
    private final By saveAsDraftButton = By.className("Ha");

    public IncomingMessagesPage(WebDriver driver) {
        super(driver);
    }

    public IncomingMessagesPage setInfo(String addresseeText, String subjectText, String bodyText) {
        driver.findElement(addressee).sendKeys(addresseeText);
        driver.findElement(subject).sendKeys(subjectText);
        driver.findElement(textBody).sendKeys(bodyText);
        return new IncomingMessagesPage(driver);
    }

    public IncomingMessagesPage createMail(String addresseeText, String subjectText, String bodyText) {
        driver.findElement(createMailButton).click();
        IncomingMessagesPage incomingMessagesPage = setInfo(addresseeText, subjectText, bodyText);
        (new WebDriverWait(driver, 40))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(string(), 'Черновик сохранен')]")));
        BaseTest baseTest = new BaseTest();
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(String.format("//div[contains(string(), '%s')]", baseTest.SUBJECT))));
        driver.findElement(saveAsDraftButton).click();
        return incomingMessagesPage;
    }
}
