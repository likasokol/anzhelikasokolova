package com.as.training.hw15.tests;

import com.as.training.hw15.businessObjects.Browser;
import com.as.training.hw15.pages.DraftsPage;
import com.as.training.hw15.pages.InboxPage;
import com.as.training.hw15.pages.SentPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SentTest extends BaseTest {
    @Test
    public void testMailIsPresent() {
        new InboxPage ( Browser.getInstance () ).createMail ( mail ).saveAsDraft ( mail );
        new DraftsPage ( Browser.getInstance () ).goToDrafts ().openMail ().sendMail ();
        SentPage sentPage = new SentPage(Browser.getInstance()).goToSent();
        Assert.assertTrue(Browser.getInstance().findElement(sentPage.mail).isDisplayed());
    }
}

