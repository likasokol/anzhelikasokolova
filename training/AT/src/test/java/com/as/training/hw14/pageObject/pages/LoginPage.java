package com.as.training.hw14.pageObject.pages;

import com.as.training.hw14.pageObject.businessObjects.User;
import com.as.training.hw14.pageObject.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {
    private final By loginField = By.id("identifierId");
    private final By passwordField = By.name("password");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public IncomingMessagesPage login(User user) {
        setLogin(user);
        driver.findElement(loginField).sendKeys(Keys.ENTER);
        setPassword(user);
        driver.findElement(passwordField).sendKeys(Keys.ENTER);
        return new IncomingMessagesPage(driver);
    }

    public IncomingMessagesPage setLogin(User user) {
        Waiter.waitForElementClickable(loginField);
        driver.findElement(loginField).sendKeys(user.getName());
        return new IncomingMessagesPage(driver);
    }

    public IncomingMessagesPage setPassword(User user) {
        Waiter.waitForElementClickable(passwordField);
        driver.findElement(passwordField).sendKeys(user.getPassword());
        return new IncomingMessagesPage(driver);
    }
}
