package com.as.training.hw14.pageObject.pages;

import com.as.training.hw14.pageObject.businessObjects.Mail;
import com.as.training.hw14.pageObject.tests.BaseTest;
import com.as.training.hw14.pageObject.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class IncomingMessagesPage extends Page {
    public final By createMailButton = By.xpath ( "//div[@class='T-I J-J5-Ji T-I-KE L3']" );
    private final By addressee = By.name ( "to" );
    private final By subject = By.name ( "subjectbox" );
    private final By textBody = By.cssSelector ( "div[class='Am Al editable LW-avf']" );
    private final By saveAsDraftButton = By.className ( "Ha" );

    public IncomingMessagesPage(WebDriver driver) {
        super ( driver );
    }

    public IncomingMessagesPage createMail(Mail mail) {
        driver.findElement ( createMailButton ).click ();
        return setInfo ( mail );
    }

    public IncomingMessagesPage saveAsDraft() {
        Waiter.waitForElementVisible ( By.xpath ( "//h2[@class='a3E zu']" ) );
        Waiter.waitForElementVisible ( By.xpath ( String.format ( "//div[contains(string(), '%s')]", BaseTest.SUBJECT ) ) );
        driver.findElement ( saveAsDraftButton ).click ();
        return new IncomingMessagesPage ( driver );
    }

    public IncomingMessagesPage setInfo(Mail mail) {
        Waiter.waitForElementVisible ( addressee );
        driver.findElement ( addressee ).click ();
        driver.findElement ( addressee ).sendKeys ( mail.getAddressee () );
        driver.findElement ( subject ).sendKeys ( mail.getSubject () );
        driver.findElement ( textBody ).sendKeys ( mail.getBody () );
        return new IncomingMessagesPage ( driver );
    }
}
