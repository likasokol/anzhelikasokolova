package com.as.training.hw15.pages;

import com.as.training.hw15.tests.BaseTest;
import com.as.training.hw15.utils.Waiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentPage extends Page {
    private final By sent = By.xpath ( "//div[@class='TN bzz aHS-bnu']" );
    public final By mail = By.xpath ( String.format ( "//span[@class='bog']//span[contains(text(),'%s')]", BaseTest.mail.getSubject () ) );

    public SentPage goToSent() {
        Waiter.waitForElementVisible ( sent );
        driver.findElement ( sent ).click ();
        Waiter.waitForElementVisible ( mail );
        return new SentPage ( driver );
    }

    public SentPage(WebDriver driver) {
        super ( driver );
    }
}