package com.as.training.hw12.pageFactory.tests;

import com.as.training.hw12.pageFactory.pages.DraftsPage;
import com.as.training.hw12.pageFactory.pages.SentPage;
import com.as.training.hw12.pageObject.pages.IncomingMessagesPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class SentTest extends BaseTest {
    @Test
    public void testMailIsPresentInSent() {
        new IncomingMessagesPage(driver).createMail(ADDRESSEE, SUBJECT, BODY);
        new DraftsPage(driver).goToDrafts().openMail();
        new DraftsPage(driver).sendMail();
        SentPage sentPage = new SentPage(driver).goToSent();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime time = LocalTime.now();
        String f = formatter.format(time);
        Assert.assertEquals(sentPage.getTime(), f);
    }
}
