@great
Feature: Navigate to product page

  Scenario: Navigate to product page
    Given I opened Ebay Page
    When I search the product "samsung 850 pro"
    And I navigate to product page
    Then the product page is opened

  Scenario: Add product to Cart
    Given I opened Ebay Page
    When I search the product "samsung 850 pro"
    And I navigate to product page
    And I add product to cart
    Then the term query "Samsung 850 Pro" should be the first in the Cart grid