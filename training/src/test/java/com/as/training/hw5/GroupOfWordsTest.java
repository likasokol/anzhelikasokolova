package com.as.training.hw5;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class GroupOfWordsTest {
    @Test
    public void testGetUniqueWords() {
        String text = "a, a b...b";
        Set<String> uniqueWords = GroupOfWords.getUniqueWords(text);
        Assert.assertEquals(Set.of("a", "b"), uniqueWords);
    }
}
