package com.as.training.hw4.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AtmTest {
    private Atm atm;

    @Before
    public void init() {
        Card creditCard = new CreditCard("Lika");
        atm = new Atm();
        atm.setCard(creditCard);
    }

    @Test
    public void testReduceBalance() {
        double result = atm.reduceBalance(400);
        Assert.assertEquals(-400, result, 0);
    }

    @Test
    public void testReplenishBalance() {
        double result = atm.replenishBalance(400);
        Assert.assertEquals(400, result, 0);
    }
}
