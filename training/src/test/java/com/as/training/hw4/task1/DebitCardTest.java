package com.as.training.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

public class DebitCardTest {
    @Test
    public void testReduceBalance() {
        Card debitCard = new DebitCard("Lika", 399.9);
        double result = debitCard.reduceBalance(400);
        Assert.assertEquals(399.9, result, 0);
    }

    @Test
    public void testReduceBalanceSuccesfully() {
        Card debitCard = new DebitCard("Lika", 400);
        double result = debitCard.reduceBalance(200);
        Assert.assertEquals(200, result, 0);
    }
}
