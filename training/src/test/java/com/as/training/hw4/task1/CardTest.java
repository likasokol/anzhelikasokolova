package com.as.training.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

public class CardTest {
    @Test
    public void testGetBalance() {
        Card card = new Card("Lika");
        double result = card.getBalance();
        Assert.assertEquals(0.0, result, 0);
    }

    @Test
    public void testReplenishBalance() {
        Card card = new Card("Lika", 400);
        double result = card.replenishBalance(300.4);
        Assert.assertEquals(700.4, result, 0);
    }

    @Test
    public void testGetBalanceInAnotherCurrency() {
        Card card = new Card("Lika", 4.44);
        double result = card.getBalanceInAnotherCurrency(57.23);
        Assert.assertEquals(254.1012, result, 0);
    }
}
