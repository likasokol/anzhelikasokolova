package com.as.training.hw4.task1;

import org.junit.Assert;
import org.junit.Test;

public class CreditCardTest {
    @Test
    public void testReduceBalance() {
        Card creditCard = new CreditCard("Lika");
        double result = creditCard.reduceBalance(400);
        Assert.assertEquals(-400, result, 0);
    }
}
