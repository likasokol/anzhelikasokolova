package com.as.training.hw7.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

public class MySetTest {
    MySet mySet;
    HashSet hashSet1;
    HashSet hashSet2;

    @Before
    public void initial() {
        hashSet1 = new HashSet();
        hashSet1.add(1);
        hashSet1.add(2);
        hashSet2 = new HashSet();
        hashSet2.add(2);
        hashSet2.add(3);
        mySet = new MySet();
    }

    @Test
    public void union() {
        HashSet result = mySet.union(hashSet1, hashSet2);
        HashSet result1 = new HashSet();
        result1.add(1);
        result1.add(2);
        result1.add(3);
        Assert.assertTrue(result.equals(result1));
    }

    @Test
    public void intersection() {
        HashSet result = mySet.intersection(hashSet1, hashSet2);
        HashSet result1 = new HashSet();
        result1.add(2);
        Assert.assertTrue(result.equals(result1));
    }

    @Test
    public void difference() {
        HashSet result = mySet.difference(hashSet1, hashSet2);
        HashSet result1 = new HashSet();
        result1.add(1);
        Assert.assertTrue(result.equals(result1));
    }

    @Test
    public void symmetricDifference() {
        HashSet result = mySet.symmetricDifference(hashSet1, hashSet2);
        HashSet result1 = new HashSet();
        result1.add(1);
        result1.add(3);
        Assert.assertTrue(result.equals(result1));
    }
}
