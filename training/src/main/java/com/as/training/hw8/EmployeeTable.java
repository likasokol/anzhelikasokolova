package com.as.training.hw8;

import java.io.Serializable;
import java.util.ArrayList;

public class EmployeeTable implements Serializable {
    private ArrayList<Employee> employees;

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public EmployeeTable() {
        employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        if (employees.stream().filter(e -> e.getId() == employee.getId()).findFirst().isPresent()) {
            System.out.println("There is employee in the table with Id = " + employee.getId());
            return;
        }
        employees.add(employee);
    }

    public void removeEmployee(int id) {
        employees.removeIf(i -> (i.getId() == id));
    }

    public void print() {
        for (Employee employee : employees) {
            System.out.println(employee.getId() + " " + employee.getFirstName() + " " + employee.getLastName());
        }
    }
}