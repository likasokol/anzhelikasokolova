package com.as.training.hw8;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class WorkWithEmployeeTable implements Serializable {

    private EmployeeTable employeeTable;

    private void writeToFile() {
        ObjectOutputStream os = null;
        try {
            FileOutputStream fs = new FileOutputStream("EmployeeTable.dat");
            os = new ObjectOutputStream(fs);
            os.writeObject(employeeTable);
            os.close();
            ObjectOutputStream os2 = new ObjectOutputStream(new FileOutputStream("EmployeeTable.dat", true)) {
                protected void writeStreamHeader() throws IOException {
                    reset();
                }
            };
            os2.writeObject(employeeTable);
            os2.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void readFromFile() {
        ObjectInputStream ois = null;
        try {
            FileInputStream fis = new FileInputStream("EmployeeTable.dat");
            ois = new ObjectInputStream(fis);
            employeeTable = (EmployeeTable) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        WorkWithEmployeeTable w = new WorkWithEmployeeTable();
        w.employeeTable = new EmployeeTable();
        boolean flag = true;
        System.out.println("Choose action");
        System.out.println("1 - Add employee");
        System.out.println("2 - Remove employee by id");
        System.out.println("3 - Print employee table");
        System.out.println("4 - Save data");
        System.out.println("5 - Load data");
        System.out.println("6 - Exit");
        Scanner scannerInt = new Scanner(System.in);
        Scanner scannerString = new Scanner(System.in);
        while (flag) {
            int action = scannerInt.nextInt();
            int id = 0;
            switch (action) {
                case 1:
                    System.out.println("Enter id");
                    boolean isException = true;
                    do {
                        try {
                            id = scannerInt.nextInt();
                            isException = false;
                        } catch (InputMismatchException e) {
                            System.out.println("Incorrect type !");
                            scannerInt.next();
                        }
                    } while (isException);
                    System.out.println("Enter first name");
                    String firstName = scannerString.nextLine();
                    System.out.println("Enter last name");
                    String lastName = scannerString.nextLine();
                    Employee employee = new Employee(id, firstName, lastName);
                    w.employeeTable.addEmployee(employee);
                    w.employeeTable.print();
                    break;
                case 2:
                    System.out.println("Enter id");
                    int id1 = scannerInt.nextInt();
                    w.employeeTable.removeEmployee(id1);
                    w.employeeTable.print();
                    break;
                case 3:
                    w.employeeTable.print();
                    break;
                case 4:
                    w.writeToFile();
                    return;
                case 5:
                    w.readFromFile();
                    break;
                case 6:
                    return;
                default:
                    System.out.println("Incorrect operation");
                    break;
            }
        }
    }
}