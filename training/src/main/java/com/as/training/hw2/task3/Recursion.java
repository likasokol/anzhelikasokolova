package com.as.training.hw2.task3;

public class Recursion {
    public static int[] fibIntArray = new int[Short.MAX_VALUE];
    public static long[] fibLongArray = new long[Short.MAX_VALUE];

    public static int getMaxFibonacciNumberForInt() {
        int maxNumber = 0;
        while (getFibonacciForInt(maxNumber) >= 0) {
            maxNumber++;
        }
        return maxNumber - 1;
    }

    public static long getMaxFibonacciNumberForLong() {
        int maxNumber = 0;
        while (getFibonacciForLong(maxNumber) >= 0) {
            maxNumber++;
        }
        return maxNumber - 1;
    }

    public static int getFibonacciForInt(int n) {
        int fibValue = 0;
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else if (fibIntArray[n] != 0) {
            return fibIntArray[n];
        } else {
            fibValue = getFibonacciForInt(n - 1) + getFibonacciForInt(n - 2);
            fibIntArray[n] = fibValue;
            return fibValue;
        }
    }

    public static Long getFibonacciForLong(long n) {
        long fibValue = 0;
        if (n == 0) {
            return (long) 0;
        } else if (n == 1) {
            return (long) 1;
        } else if (fibLongArray[(int) n] != 0) {
            return fibLongArray[(int) n];
        } else {
            fibValue = getFibonacciForLong(n - 1) + getFibonacciForLong(n - 2);
            fibLongArray[(int) n] = fibValue;
            return fibValue;
        }
    }

    public static void main(String[] args) {
        System.out.println(getMaxFibonacciNumberForInt());
        System.out.println(getMaxFibonacciNumberForLong());
    }
}