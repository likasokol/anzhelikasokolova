package com.as.training.hw2.task2;

public class Sequence {
    public String usualVariable = initialize("usual variable");
    public static String staticVariable = initialize("static variable");

    static String initialize(String name) {
        System.out.println(name);
        return name;
    }

    {
        System.out.println("initialization block 1");
    }

    {
        System.out.println("initialization block 2");
    }

    static {
        System.out.println("static block 1");
    }

    static {
        System.out.println("static block 2");
    }

    public static void main(String[] args) {
        Sequence m;
        Sequence m1 = new Sequence();
        System.out.println("Assignment reference to the variable " + (m = m1));
    }
}
