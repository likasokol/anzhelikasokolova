package com.as.training.hw2.task1;

import java.util.ArrayList;

public class Initializer {
    static byte byteValue;
    static short shortValue;
    static int intValue;
    static long longValue;
    static float floatValue;
    static double doubleValue;
    static char charValue;
    static boolean booleanValue;
    static String stringValue;
    static ArrayList<Integer> arrayValue;

    public static void main(String[] args) {
        System.out.println("Default value for byte " + byteValue);
        System.out.println("Default value for short " + shortValue);
        System.out.println("Default value for int " + intValue);
        System.out.println("Default value for long " + longValue);
        System.out.println("Default value for float " + floatValue);
        System.out.println("Default value for double " + doubleValue);
        System.out.println("Default value for char " + charValue);
        System.out.println("Default value for boolean " + booleanValue);
        System.out.println("Default value for String " + stringValue);
        System.out.println("Default value for ArrayList " + arrayValue);
    }
}
