package com.as.training.hw4.task1;

public class DebitCard extends Card {
    public DebitCard(String ownerName) {
        super(ownerName);
    }

    public DebitCard(String ownerName, double balance) {
        super(ownerName, balance);
    }

    @Override
    public double reduceBalance(double sum) {
        if ((getBalance() - sum) < 0) {
            System.out.println("You do not have enough money " + getBalance());
            return getBalance();
        }
        return super.reduceBalance(sum);
    }
}