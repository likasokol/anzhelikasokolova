package com.as.training.hw4.task2;

public class SortExecutor {
    public static void main(String[] args) {
        int[] array = {4, 3, 2, 8};
        if (args.length < 1) {
            System.out.println("No arguments");
            return;
        }
        Sorter sortStrategy = null;
        switch (args[0]) {
            case "BubbleSort":
                sortStrategy = new BubbleSort();
                break;
            case "SelectionSort":
                sortStrategy = new SelectionSort();
                break;
            default:
                System.out.println("Incorrect argument");
                return;
        }
        SortingContext context = new SortingContext(sortStrategy);
        context.execute(array);
    }
}

