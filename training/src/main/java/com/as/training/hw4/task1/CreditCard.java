package com.as.training.hw4.task1;

public class CreditCard extends Card {
    public CreditCard(String ownerName) {
        super(ownerName);
    }

    public CreditCard(String ownerName, double balance) {
        super(ownerName, balance);
    }
}
