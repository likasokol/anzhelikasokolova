package com.as.training.hw4.task1;

public class Card {
    private String ownerName;
    private double balance;

    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public double getBalance() {
        return balance;
    }

    public double replenishBalance(double sum) {
        balance += sum;
        System.out.println(balance);
        return balance;
    }

    public double reduceBalance(double sum) {
        balance -= sum;
        System.out.println(balance);
        return balance;
    }

    public double getBalanceInAnotherCurrency(double conversionRate) {
        double balanceInAnotherCurrency = balance * conversionRate;
        System.out.println(balanceInAnotherCurrency);
        return balanceInAnotherCurrency;
    }
}

