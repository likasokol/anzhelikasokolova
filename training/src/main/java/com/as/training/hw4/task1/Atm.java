package com.as.training.hw4.task1;

public class Atm {
    private Card card;

    public void setCard(Card card) {
        this.card = card;
    }

    public double reduceBalance(double sum) {
        return card.reduceBalance(sum);
    }

    public double replenishBalance(double sum) {
        return card.replenishBalance(sum);
    }
}
