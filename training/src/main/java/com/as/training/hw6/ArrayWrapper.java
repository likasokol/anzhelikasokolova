package com.as.training.hw6;

public class ArrayWrapper<T> {
    private T[] array;

    public ArrayWrapper(T[] array) {
        this.array = array;
    }

    public T get(int index) {
        if (index < 1 || index > array.length) {
            throw new IncorrectArrayWrapperIndex("Incorrect index");
        }
        return array[index - 1];
    }

    public boolean replace(int i, T newElement) {
        if (i < 1 || i > array.length) {
            throw new IncorrectArrayWrapperIndex("Incorrect index");
        }
        if (newElement instanceof String) {
            if (((String) newElement).length() > ((String) array[i - 1]).length()) {
                array[i - 1] = newElement;
                return true;
            } else {
                return false;
            }
        } else if (newElement instanceof Integer) {
            if ((Integer) newElement > (Integer) array[i - 1]) {
                array[i - 1] = newElement;
                return true;
            } else {
                return false;
            }
        } else {
            array[i - 1] = newElement;
            return true;
        }
    }
}
