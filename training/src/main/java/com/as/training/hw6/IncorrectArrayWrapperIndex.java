package com.as.training.hw6;

public class IncorrectArrayWrapperIndex extends RuntimeException {
    IncorrectArrayWrapperIndex(String message) {
        super(message);
    }
}
