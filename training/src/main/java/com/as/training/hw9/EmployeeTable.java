package com.as.training.hw9;

import java.sql.*;
import java.util.Scanner;

public class EmployeeTable {
    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        Scanner scanner = new Scanner(System.in);
        boolean flag = true;
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:src/main/resources/db/db_for_m4_l3.db");
            statement = connection.createStatement();
            System.out.println("1 - Print info\n2 - Add employee\n3 - Remove employee\n4 - Exit");
            while (flag) {
                System.out.println("Choose action");
                int action = scanner.nextInt();
                switch (action) {
                    case 1:
                        printEmployeesInfo(statement);
                        break;
                    case 2:
                        System.out.println("Enter first name");
                        String firstName = scanner.next();
                        System.out.println("Enter last name");
                        String lastName = scanner.next();
                        Employee employee = new Employee(firstName, lastName);
                        addEmployee(employee, statement);
                        break;
                    case 3:
                        System.out.println("Enter id");
                        int id = scanner.nextInt();
                        removeEmployee(id, statement);
                        break;
                    case 4:
                        return;
                    default:
                        System.out.println("incorrect action");
                        break;
                }
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void printEmployeesInfo(Statement statement) {
        ResultSet resultSet = null;
        try {
            resultSet = statement.executeQuery("select * from Employees");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (true) {
            String id = null;
            String firstname = null;
            String lastname = null;
            try {
                if (!resultSet.next()) {
                    break;
                }
                id = resultSet.getString(1);
                firstname = resultSet.getString(2);
                lastname = resultSet.getString(3);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println(id + " " + firstname + " " + lastname);
        }
    }

    private static void addEmployee(Employee employee, Statement statement) {
        try {
            statement.executeUpdate(String.format("INSERT INTO Employees (firstName,lastName) VALUES ('%s','%s')", employee.getFirstName(), employee.getLastName()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void removeEmployee(int id, Statement statement) {
        try {
            statement.executeUpdate("DELETE from Employees where id = " + id + "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
