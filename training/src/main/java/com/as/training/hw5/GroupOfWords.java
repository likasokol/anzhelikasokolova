package com.as.training.hw5;

import java.util.*;

public class GroupOfWords {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        printUniqueWords(getUniqueWords(text));
    }

    public static Set<String> getUniqueWords(String text) {
        String[] words = text.toLowerCase().split("\\W+");
        return new TreeSet<>(Arrays.asList(words));
    }

    public static void printUniqueWords(Set<String> uniqueWords) {
        char prevC = '\u0000';
        char nextC;
        for (String i : uniqueWords) {
            nextC = i.charAt(0);
            if (nextC != prevC) {
                System.out.println();
                System.out.print(Character.toUpperCase(nextC) + ": ");
            }
            System.out.print(i + " ");
            prevC = nextC;
        }
    }
}
