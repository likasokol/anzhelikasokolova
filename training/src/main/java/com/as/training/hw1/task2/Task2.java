package com.as.training.hw1.task2;

public class Task2 {
    public static void main(String[] args) {
        int algorithmId = Integer.parseInt(args[0]);
        int loopType = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);

        if (algorithmId == 1) {
            calculateFibonacciNumbers(loopType, n);
        } else if (algorithmId == 2) {
            System.out.println(calculateFactorial(loopType, n));
        }
    }

    public static void calculateFibonacciNumbers(int loopType, int n) {
        int previousPreviousValue = 1;
        int previousValue = 1;
        int currentValue;
        if (n == 1) {
            System.out.print(previousPreviousValue + " ");
        } else {
            System.out.print(previousPreviousValue + " " + previousValue + " ");

            if (loopType == 1) {
                int i = 3;
                while (i <= n) {
                    currentValue = previousPreviousValue + previousValue;
                    System.out.print(currentValue + " ");
                    previousPreviousValue = previousValue;
                    previousValue = currentValue;

                    i++;
                }
            } else if (loopType == 2) {
                int i = 3;
                do {
                    if (n == 2) {
                        break;
                    }
                    currentValue = previousPreviousValue + previousValue;
                    System.out.print(currentValue + " ");
                    previousPreviousValue = previousValue;
                    previousValue = currentValue;
                    i++;
                } while (i <= n);
            } else if (loopType == 3) {
                for (int i = 3; i <= n; i++) {
                    currentValue = previousPreviousValue + previousValue;
                    System.out.print(currentValue + " ");
                    previousPreviousValue = previousValue;
                    previousValue = currentValue;
                }
            }
        }
    }

    public static int calculateFactorial(int loopType, int n) {
        int result = 1;
        int i = 1;

        if (loopType == 1) {
            while (i <= n) {
                result = result * i;
                i++;
            }
        } else if (loopType == 2) {
            do {
                result = result * i;
                i++;
            } while (i <= n);
        } else if (loopType == 3) {
            for (i = 1; i <= n; i++) {
                result = result * i;
            }
        }
        return result;
    }
}
