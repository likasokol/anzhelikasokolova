package com.as.training.hw7.task1;

import java.util.Arrays;

public class Sorter {
    private static int sumOfDigits(int digits) {
        int sum = 0;
        if (digits < 0) {
            digits = -digits;
        }
        while (digits != 0) {
            sum += (digits % 10);
            digits /= 10;
        }
        return sum;
    }

    private static int[] sortBySumOfDigits(int[] arr) {
        for (int i = 1; i < arr.length; i++)
            if (sumOfDigits(arr[i]) < sumOfDigits(arr[i - 1])) {
                int temp = arr[i];
                arr[i] = arr[i - 1];
                arr[i - 1] = temp;
            }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{-144,10};
        System.out.println("INPUT = " + Arrays.toString(arr));
        System.out.println("OUTPUT = " + Arrays.toString(sortBySumOfDigits(arr)));
    }
}
