package com.as.training.hw7.task2;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class MySet<T> extends HashSet {

    public HashSet<Integer> union(HashSet<Integer> set1, HashSet<Integer> set2) {
         set1.addAll(set2);
         return set1;
    }

    public HashSet intersection(HashSet<Integer> set1, HashSet<Integer> set2) {
        set1.retainAll(set2);
        return set1;
    }

    public HashSet difference(HashSet<Integer> set1, HashSet<Integer> set2) {
        set1.removeAll(set2);
        return set1;
    }

    public HashSet symmetricDifference(HashSet<Integer> set1, HashSet<Integer> set2) {
        HashSet<Integer> result1 = new HashSet<>();
        HashSet<Integer> result2 = new HashSet<>();
        result1.addAll(set1);
        result1.removeAll(set2);

        result2.addAll(set2);
        result2.removeAll(set1);

        result1.addAll(result2);

        return result1;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        return this.equals(object);
    }

    @Override
    public int hashCode() {
        return this.hashCode();
    }
}
